'use strict'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const port = process.env.PORT || 3100;
const URL_VEHICULOS = 'https://localhost:3000/api/vehiculos';
const URL_VUELOS = 'https://localhost:3002/api/vuelos'; 
const URL_HOTELES = 'https://localhost:3001/api/hoteles'; 
const URL_USUARIOS = 'https://localhost:3006/api/usuarios';
const URL_PAGOS = 'https://localhost:3003/api/pagos';
const URL_TRANSACCIONES = 'https://localhost:3008/api/transacciones';

const https = require('https');
const fs = require('fs');
const moment = require('moment');

const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const usuario = {
    email:'',
    displayName:'',
    password:''
}

const express = require('express');  
const logger = require('morgan');
const fetch = require('node-fetch');
const app = express();


//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//Middleware para la autorización (seguridad)
function auth(req, res, next) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];
    console.log(miToken);
    tokenService.decodificarToken(miToken)
    .then( userID => {

        req.user = {
            userId: userID,
            token: miToken
        }
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensaje: 'Acceso no autorizado'
            });

            return next(new Error("Acceso no autorizado"));
        });
}

//Mostrar todos los elementos de una determinada tabla
//al crear un elemento por primera vez en una tabla la 
//tabla se crea automáticamente sin necesidad de haberla
//creado antes
app.get('/api/vehiculos', (req, res, next) => {
    const queNombre = req.query.Nombre;
    const queMarca = req.query.Marca;
    const params = req.query;
    let queURL;

    if(JSON.stringify(params) == '{}'){
        queURL = `${URL_VEHICULOS}`;
    }
    else{
        queURL = `${URL_VEHICULOS}?Nombre=${queNombre}&Marca=${queMarca}`
    }
    console.log(queURL);
    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                colecciones: 'vehiculos',
                elemento: json.elemento
            });
    });
});

app.get('/api/vuelos', (req, res, next) => {
    const queDestino = req.query.Destino;
    const queAerolinea = req.query.Aerolinea;
    const params = req.query;
    let queURL;

    if(JSON.stringify(params) == '{}'){
        queURL = `${URL_VUELOS}`;
    }
    else{
        queURL = `${URL_VUELOS}?Destino=${queDestino}&Aerolinea=${queAerolinea}`
    }
    console.log(queURL);
    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                colecciones: 'vuelos',
                elemento: json.elemento
            });
    });
});

app.get('/api/hoteles', (req, res, next) => {
    const queNombre = req.query.Nombre;
    const queLugar = req.query.Lugar;
    const params = req.query;
    let queURL;

    if(JSON.stringify(params) == '{}'){
        queURL = `${URL_HOTELES}`;
    }
    else{
        queURL = `${URL_HOTELES}?Nombre=${queNombre}&Lugar=${queLugar}`
    }
    console.log(queURL);
    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                colecciones: 'hoteles',
                elemento: json.elemento
            });
    });
});

app.get('/api/usuarios', (req, res, next) => {
    const queURL = `${URL_USUARIOS}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                colecciones: 'usuarios',
                elementos: json.elementos
            });
    });
});

app.get('/api/pagos', (req, res, next) => {
    const queURL = `${URL_PAGOS}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then (json => {
            res.json({
                result: json.result
            });
        });
});

app.get('/api/transacciones', (req, res, next) => {
    const queURL = `${URL_TRANSACCIONES}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                colecciones: 'transacciones',
                elementos: json.elementos
            });
    });
});

//Devolver un elemento concreto de una tabla concreta
app.get('/api/vehiculos/:id', (req, res, next) => {
    const queId = req.params.id;
    const queURL = `${URL_VEHICULOS}/${queId}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'vehiculos',
                elementos: json.elemento
            });
    });
});

app.get('/api/vuelos/:id', (req, res, next) => {
    const queId = req.params.id
    const queURL = `${URL_VUELOS}/${queId}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'vuelos',
                elementos: json.elemento
            });
    });
});

app.get('/api/hoteles/:id', (req, res, next) => {
    const queId = req.params.id
    const queURL = `${URL_HOTELES}/${queId}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'hoteles',
                elementos: json.elemento
            });
    });
});

app.get('/api/transacciones/:id', (req, res, next) => {
    const queId = req.params.id
    const queURL = `${URL_TRANSACCIONES}/${queId}`;

    fetch(queURL)
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'transacciones',
                elementos: json.elemento
            });
    });
});

//Añadir elementos a una colección(tabla)
app.post('/api/vehiculos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VEHICULOS}`;

    fetch(queURL,{
            method: 'POST',
            body: JSON.stringify(nuevoElemento),
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'vehiculos',
                elementos: json.elemento
            });
    });
});

app.post('/api/vuelos', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_VUELOS}`;

    fetch(queURL,{
            method: 'POST',
            body: JSON.stringify(nuevoElemento),
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'vuelos',
                elementos: json.elemento
            });
    });
});

app.post('/api/hoteles', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_HOTELES}`;

    fetch(queURL,{
            method: 'POST',
            body: JSON.stringify(nuevoElemento),
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'hoteles',
                elementos: json.elemento
            });
    });
});

app.post('/api/usuarios', (req, res, next) => {
    const queURL = `${URL_USUARIOS}`;
    const elemento = req.body;
    
    usuario.email = req.body.email;
    usuario.displayName = req.body.displayName; 
    usuario.password = elemento.password;
    
    passService.encriptaPassword(usuario.password)
    .then(hash => {
        elemento.password = hash;
    });

    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json'}
                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            elemento: mijson.elemento,
            token: tokenService.crearToken(usuario)
        });
    });
});

app.post('/api/transacciones', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_TRANSACCIONES}`;

    fetch(queURL,{
            method: 'POST',
            body: JSON.stringify(nuevoElemento),
            headers: {
                'Content-Type' : 'application/json',
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'transacciones',
                elementos: json.elemento
            });
    });
});

app.post('/api/transacciones/reservarVehiculo/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    
    const elemento = {
        Primer_Paso: 'Inicio de transaccion',
        InicioTiempo: moment().unix(),
        id: queId
    }
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_TRANSACCIONES}/reservarVehiculo`;

    console.log(elemento);
    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'transacciones',
            elemento: mijson.elementos
        });
    });
});

app.post('/api/transacciones/reservarHotel/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    
    const elemento = {
        Primer_Paso: 'Inicio de transaccion',
        InicioTiempo: moment().unix(),
        id: queId
    }
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_TRANSACCIONES}/reservarHotel`;

    console.log(elemento);
    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'transacciones',
            elemento: mijson.elementos
        });
    });
});

app.post('/api/transacciones/reservarVuelo/:id', auth, (req, res, next) => {
    const queId = req.params.id;
    
    const elemento = {
        Primer_Paso: 'Inicio de transaccion',
        InicioTiempo: moment().unix(),
        id: queId
    }
    const queToken = req.headers.authorization.split(" ")[1];
    const queURL = `${URL_TRANSACCIONES}/reservarVuelo`;

    console.log(elemento);
    fetch(queURL, {
                    method: 'POST',
                    body: JSON.stringify(elemento),
                    headers: {'Content-Type': 'application/json',
                              'Authorization': `Bearer ${queToken}`                                   
                             }

                    })
    .then( res => res.json() )
    .then( mijson => {
        res.json({
            result: 'OK',
            coleccion: 'transacciones',
            elemento: mijson.elementos
        });
    });
});

app.put('/api/vehiculos/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;
    const queURL = `${URL_VEHICULOS}/${queId}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    }).then(resp => resp.json()).then(json => {
        res.json({
            result: 'OK',
            colecciones: 'vehiculos',
            resultados: json.result
        });
    });
});

app.put('/api/vuelos/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;
    const queURL = `${URL_VUELOS}/${queId}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    }).then(resp => resp.json()).then(json => {
        res.json({
            result: 'OK',
            colecciones: 'vuelos',
            resultados: json.result
        });
    });
});

app.put('/api/hoteles/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;
    const queURL = `${URL_HOTELES}/${queId}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    }).then(resp => resp.json()).then(json => {
        res.json({
            result: 'OK',
            colecciones: 'hoteles',
            resultados: json.result
        });
    });
});

app.put('/api/transacciones/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;
    const queURL = `${URL_TRANSACCIONES}/${queId}`;
    const queToken = req.headers.authorization.split(" ")[1];

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    }).then(resp => resp.json()).then(json => {
        res.json({
            result: 'OK',
            colecciones: 'transacciones',
            resultados: json.result
        });
    });
});

app.put('/api/usuarios', (req, res, next) => {
    const queURL = `${URL_USUARIOS}?email=${req.body.email}`;

    fetch(queURL)
    .then( res => res.json() )
    .then( mijson => {

        if(mijson.Elemento == null)
        {
            res.status(400).json({
                result: 'No existe el usuario'
            })
        }
        else
        {
            let elemento = 
                {
                    email: mijson.Elemento.email,
                    password: mijson.Elemento.password,
                    token: mijson.Elemento.token,
                    lastLogin: moment().format('DD/MM/YYYY HH:mm')
                }

            console.log(req.body.password);
            console.log(elemento.password);
            passService.comparaPassword(req.body.password, elemento.password)
            .then(isOk => {
                if(isOk) {
                    fetch(queURL, {
                        method: 'PUT',
                        body: JSON.stringify(elemento),
                        headers: {
                            'Content-Type': 'application/json',
                                }

                        })
                    .then( resp => resp.json() )
                    .then( mijson => {
                            res.json({
                            result: 'OK',
                            token: elemento.token
                        });
                    });

                }
                else{
                    res.status(400).json({
                        result: 'Credenciales incorrectas'
                    })
                }
            });
        }
    });
})

app.delete('/api/vehiculos/:id', auth, (req, res, next) => {
    const queToken = req.headers.authorization.split(" ")[1];
    const queId = req.params.id;
    const queURL = `${URL_VEHICULOS}/${queId}`;

    fetch(queURL, {
            method: 'DELETE',
            body: queId,
            headers: {
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'vehiculos',
                elementos: json.elemento
            });
    });
});

app.delete('/api/vuelos/:id', auth, (req, res, next) => {
    const queToken = req.headers.authorization.split(" ")[1];
    const queId = req.params.id;
    const queURL = `${URL_VUELOS}/${queId}`;

    fetch(queURL, {
            method: 'DELETE',
            body: queId,
            headers: {
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'vuelos',
                elementos: json.elemento
            });
    });
});

app.delete('/api/hoteles/:id', auth, (req, res, next) => {
    const queToken = req.headers.authorization.split(" ")[1];
    const queId = req.params.id;
    const queURL = `${URL_HOTELES}/${queId}`;

    fetch(queURL, {
            method: 'DELETE',
            body: queId,
            headers: {
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'hoteles',
                elementos: json.elemento
            });
    });
});

app.delete('/api/transacciones/:id', auth, (req, res, next) => {
    const queToken = req.headers.authorization.split(" ")[1];
    const queId = req.params.id;
    const queURL = `${URL_TRANSACCIONES}/${queId}`;

    fetch(queURL, {
            method: 'DELETE',
            body: queId,
            headers: {
                'Authorization' : `Bearer ${queToken}`
            }
        })
        .then(resp => resp.json())
            .then(json => {
            //LOGICA DE NEGOCIO
            res.json({
                result: json.result,
                coleccion: 'transacciones',
                elementos: json.elemento
            });
    });
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API-GW ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});

