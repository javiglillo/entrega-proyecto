'use strict'

const port = process.env.PORT || 3003;

const https = require('https');
const fs = require('fs');
const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');  
const logger = require('morgan');
const app = express();


//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//Middleware para la autorización (seguridad)
function auth(req, res, next) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificarToken(miToken)
    .then( userID => {

        req.user = {
            userId: userID,
            token: miToken
        }
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });

            return next(new Error("Acceso no autorizado"));
        });
}

app.get('/api/pagos', (req, res, next) => {
    let porcentaje = Math.random(0,10);

    if(porcentaje < 9) //90% de pagos realizados correctamente
    {
        res.json({
            result: 'Pago realizado correctamente'
        }); 
    }
    else{
        res.json({
            result: 'Error al realizar el pago'
        });
    }

});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST DE PAGOS ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});



