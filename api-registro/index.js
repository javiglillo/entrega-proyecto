'use strict'

const port = process.env.PORT || 3006;

const https = require('https');
const fs = require('fs');
const moment = require('moment');
const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');  
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();

//Conectamos con la BD
var db = mongojs('mongodb+srv://javiglillo:12345@cluster0.44hrj.mongodb.net/AGENCIA?retryWrites=true&w=majority'); 
var id = mongojs.ObjectID;
const usuario = {
    email: '',
    displayName: '',
    password: '',
    firstLogin: '',
    token: ''
}
const coleccion = db.usuarios;

//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

//Middleware para la autorización (seguridad)
/*function auth(req, res, next) {
    if(!req.headers.authorization) {
        res.status(401).json({
            result: 'KO',
            mensaje: 'No se ha enviado el token en la cabecera token'
        });
        return next(new Error("Falta token de autorización"));
    };

    console.log(req.headers.authorization);
    if(req.headers.authorization.split(" ")[1] == "MITOKEN123456789"){
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: 'Acceso no autorizado a este servicio'
    });
    return next(new Error("Acceso no autorizado"));
}*/

//Mostrar todos los elementos de una determinada tabla
//al crear un elemento por primera vez en una tabla la 
//tabla se crea automáticamente sin necesidad de haberla
//creado antes
app.get('/api/usuarios', (req, res, next) => {

    const queEmail = req.query.email;

        coleccion.findOne({email: queEmail},(err, elemento) => {
            if (err) return next(err);

            res.json({
                Elemento: elemento
            });
        });
});

//Devolver un elemento concreto de una tabla concreta
app.get('/api/usuarios/:id', (req, res, next) => {
    const queId = req.params.id

    coleccion.findOne({_id: id(queId)},(err, elemento) => {
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: 'OK',
            colecciones: 'usuarios',
            elemento: elemento
        });
    });
});

//Añadir elementos a una colección(tabla)
app.post('/api/usuarios', (req, res, next) => {
    const elemento = req.body;
    usuario.email = req.body.email;
    usuario.displayName = req.body.displayName; 
    usuario.password = elemento.password;
    usuario.token = tokenService.crearToken(usuario);
    usuario.firstLogin = moment().format('DD/MM/YYYY HH:mm');
    passService.encriptaPassword(usuario.password)
    .then(hash => {
        usuario.password = hash;
        coleccion.save(usuario, (err, elementoGuardado) =>{
            if(err) return next(err);
    
            res.json({
                result: 'OK',
                coleccion: 'usuarios',
                elemento: elementoGuardado
            });
        });
    });
});

app.put('/api/usuarios', (req, res, next) => {
    let elementoNuevo = req.body;

    coleccion.update(
            {email: elementoNuevo.email}, 
            {$set: elementoNuevo}, 
            {safe: true, multi: false}, 
            (err, elementoModif) => {
                if(err) return next(err);

                res.json({
                    result: 'OK',
                    coleccion: req.params.colecciones,
                    resultado: elementoModif
                });
        });

});

app.delete('/api/usuarios/:id', auth, (req, res, next) => {
    let queId = req.params.id;

    coleccion.remove(
        {_id: id(queId)}, 
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'usuarios',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST DE REGISTRO ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});

