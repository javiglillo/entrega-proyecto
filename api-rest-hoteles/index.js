'use strict'

const port = process.env.PORT || 3001;

const https = require('https');
const fs = require('fs');
const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');  
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();

//Conectamos con la BD
var db = mongojs('mongodb+srv://javiglillo:12345@cluster0.44hrj.mongodb.net/HOTELES?retryWrites=true&w=majority');
var id = mongojs.ObjectID;
const coleccion = db.hoteles;

//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

//Middleware para la autorización (seguridad)
function auth(req, res, next) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificarToken(miToken)
    .then( userID => {

        req.user = {
            userId: userID,
            token: miToken
        }
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });

            return next(new Error("Acceso no autorizado"));
        });
}

//Mostrar todos los elementos de una determinada tabla
//al crear un elemento por primera vez en una tabla la 
//tabla se crea automáticamente sin necesidad de haberla
//creado antes
app.get('/api/hoteles', (req, res, next) => {
    let param = req.query;

    if(JSON.stringify(param) == '{}'){
        coleccion.find((err, elemento) => {
            if (err) return next(err);
    
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'hoteles',
                elemento: elemento
            });
        });
    }
    else{
        const queNombre = req.query.Nombre;
        const queLugar = req.query.Lugar;
    
        coleccion.findOne({Nombre: queNombre, Lugar: queLugar},(err, elemento) => {
            if (err) return next(err);
    
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'hoteles',
                elemento: elemento
            });
        });
    }
});

//Devolver un elemento concreto de una tabla concreta
app.get('/api/hoteles/:id', (req, res, next) => {
    const queId = req.params.id

    coleccion.findOne({_id: id(queId)},(err, elemento) => {
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: 'OK',
            colecciones: 'hoteles',
            elemento: elemento
        });
    });
});

//Añadir elementos a una colección(tabla)
app.post('/api/hoteles', auth, (req, res, next) => {
    const nuevoElemento = req.body;

    coleccion.save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        console.log(elementoGuardado);
        res.status(201).json({
            result: 'OK',
            colección: 'hoteles',
            elemento: elementoGuardado
        });
    });
});

app.put('/api/hoteles/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;

    coleccion.update(
        {_id: id(queId)}, 
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'hoteles',
                resultado: resultado
            });
        }
    );
});

app.delete('/api/hoteles/:id', auth, (req, res, next) => {
    const queId = req.params.id;

    coleccion.remove(
        {_id: id(queId)}, 
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'hoteles',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST DE HOTELES ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});



