'use strict'

const port = process.env.PORT || 3000;

const https = require('https');
const fs = require('fs');
const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');  
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();

//Conectamos con la BD
//var db = mongojs('localhost:27017/AGENCIA'); en local
var db = mongojs('mongodb+srv://javiglillo:12345@cluster0.44hrj.mongodb.net/VEHICULOS?retryWrites=true&w=majority'); 
var id = mongojs.ObjectID;
const coleccion = db.vehiculos;

//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//Middleware para la autorización (seguridad)
function auth(req, res, next) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificarToken(miToken)
    .then( userID => {

        req.user = {
            userId: userID,
            token: miToken
        }
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });

            return next(new Error("Acceso no autorizado"));
        });
}

//Mostrar todos los elementos de una determinada tabla
//al crear un elemento por primera vez en una tabla la 
//tabla se crea automáticamente sin necesidad de haberla
//creado antes
app.get('/api/vehiculos', (req, res, next) => {
    let param = req.query;

    if(JSON.stringify(param) == '{}'){
        coleccion.find((err, elemento) => {
            if (err) return next(err);
    
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'vehiculos',
                elemento: elemento
            });
        });
    }
    else{
        const queNombre = req.query.Nombre;
        const queMarca = req.query.Marca;

        coleccion.findOne({Nombre: queNombre, Marca: queMarca},(err, elemento) => {
            if (err) return next(err);
    
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'vehiculos',
                elemento: elemento
            });
        });
    }
});

//Devolver un elemento concreto de una tabla concreta
app.get('/api/vehiculos/:id', (req, res, next) => {
    const queId = req.params.id

    coleccion.findOne({_id: id(queId)},(err, elemento) => {
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: 'OK',
            colecciones: 'vehiculos',
            elemento: elemento
        });
    });
});

//Añadir elementos a una colección(tabla)
app.post('/api/vehiculos', auth, (req, res, next) => {
    const nuevoElemento = req.body;

    coleccion.save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        console.log(elementoGuardado);
        res.status(201).json({
            result: 'OK',
            colección: 'vehiculos',
            elemento: elementoGuardado
        });
    });
});

app.put('/api/vehiculos/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;

    coleccion.update(
        {_id: id(queId)}, 
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'vehiculos',
                resultado: resultado
            });
        }
    );
});

app.delete('/api/vehiculos/:id', auth, (req, res, next) => {
    let queId = req.params.id;

    coleccion.remove(
        {_id: id(queId)}, 
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'vehiculos',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST DE VEHICULOS ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});



