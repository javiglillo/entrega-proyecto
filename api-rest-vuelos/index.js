'use strict'

const port = process.env.PORT || 3002;

const https = require('https');
const fs = require('fs');
const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');  
const logger = require('morgan');
const mongojs = require('mongojs');
const app = express();

//Conectamos con la BD
var db = mongojs('mongodb+srv://javiglillo:12345@cluster0.44hrj.mongodb.net/VUELOS?retryWrites=true&w=majority');
var id = mongojs.ObjectID;
const coleccion = db.vuelos;

//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

//Middleware para la autorización (seguridad)
function auth(req, res, next) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificarToken(miToken)
    .then( userID => {

        req.user = {
            userId: userID,
            token: miToken
        }
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });

            return next(new Error("Acceso no autorizado"));
        });
}

//Mostrar todos los elementos de una determinada tabla
//al crear un elemento por primera vez en una tabla la 
//tabla se crea automáticamente sin necesidad de haberla
//creado antes
app.get('/api/vuelos', (req, res, next) => {
    if(JSON.stringify(req.query) == '{}'){
        coleccion.find((err, elemento) => {
            if (err) return next(err);
    
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'vuelos',
                elemento: elemento
            });
        });
    }
    else{
        const queDestino = req.query.Destino;
        const queAerolinea = req.query.Aerolinea;
        console.log(queAerolinea);
        console.log(queDestino);
    
        coleccion.findOne({Aerolinea: queAerolinea, Destino: queDestino},(err, elemento) => {
            if (err) return next(err);
    
            console.log(elemento);
            res.json({
                result: 'OK',
                colecciones: 'vuelos',
                elemento: elemento
            });
        });
    }
});

//Devolver un elemento concreto de una tabla concreta
app.get('/api/vuelos/:id', (req, res, next) => {
    const queId = req.params.id

    coleccion.findOne({_id: id(queId)},(err, elemento) => {
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: 'OK',
            colecciones: 'vuelos',
            elemento: elemento
        });
    });
});

//Añadir elementos a una colección(tabla)
app.post('/api/vuelos', auth, (req, res, next) => {
    const nuevoElemento = req.body;

    coleccion.save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        console.log(elementoGuardado);
        res.status(201).json({
            result: 'OK',
            colección: 'vuelos',
            elemento: elementoGuardado
        });
    });
});

app.put('/api/vuelos/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;

    coleccion.update(
        {_id: id(queId)}, 
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'vuelos',
                resultado: resultado
            });
        }
    );
});

app.delete('/api/vuelos/:id', auth, (req, res, next) => {
    const queId = req.params.id;

    coleccion.remove(
        {_id: id(queId)}, 
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'vuelos',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST DE VUELOS ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});




