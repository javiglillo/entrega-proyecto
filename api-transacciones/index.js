'use strict'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const port = process.env.PORT || 3008;

const https = require('https');
const fs = require('fs');
const passService = require('../auth-agencia/services/pass.service');
const tokenService = require('../auth-agencia/services/token.service');

const OPTIONS_HTTPS = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const express = require('express');  
const logger = require('morgan');
const mongojs = require('mongojs');
const fetch = require('node-fetch');
const helmet = require('helmet');
const bcrypt = require('bcrypt');
const { token } = require('morgan');
const moment = require('moment');
const app = express();

const URL_VEHICULOS = 'https://localhost:3000/api/vehiculos';
const URL_VUELOS = 'https://localhost:3002/api/vuelos'; 
const URL_HOTELES = 'https://localhost:3001/api/hoteles'; 
const URL_PAGOS = 'https://localhost:3003/api/pagos';

//Conectamos con la BD
var db = mongojs('mongodb+srv://javiglillo:12345@cluster0.44hrj.mongodb.net/TRANSACCIONES?retryWrites=true&w=majority'); 
var id = mongojs.ObjectID;
const coleccion = db.transacciones;

//Declaracion de los Middlewares
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());


//Middleware para la autorización (seguridad)
function auth(req, res, next) {
    if(!req.headers.authorization){
        res.status(401).json({
            result: 'KO',
            mensajes: ''
        });
        return next(new Error("Introduce token"));
    } 

    const miToken = req.headers.authorization.split(" ")[1];
    tokenService.decodificarToken(miToken)
    .then( userID => {

        req.user = {
            userId: userID,
            token: miToken
        }
        return next();
    })
    .catch(err =>
        {
            res.status(401).json({
                result: 'KO',
                mensajes: 'Acceso no autorizado'
            });

            return next(new Error("Acceso no autorizado"));
        });
}

//Mostrar todos los elementos de una determinada tabla
//al crear un elemento por primera vez en una tabla la 
//tabla se crea automáticamente sin necesidad de haberla
//creado antes
app.get('/api/transacciones', (req, res, next) => {
    
    coleccion.find((err, elementos) => {
        if (err) return next(err);

        console.log(elementos);
        res.json({
            result: 'OK',
            colecciones: 'transacciones',
            elementos: elementos
        });
    });
});

app.post('/api/transacciones/reservarVehiculo', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = req.body.id;

    const prerreservado = {estado: 'prerreservado'};
    const reservado = {estado: 'reservado'};
    const libre = {estado: 'libre'};
    const queURLV = `${URL_VEHICULOS}/${idElemento}`;
    const queURLP = `${URL_PAGOS}`;
    const queToken = req.headers.authorization.split(" ")[1];

    console.log(elemento);
    //Añado el estado prerreservado al vehiculo dado por el idElemento
    fetch(queURLV, {
        method: 'PUT',
        body: JSON.stringify(prerreservado),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`                                   
        }

    }).then( resp => resp.json() )

    fetch(queURLP)
    .then(resp => resp.json() )
    .then(mijson => 
    {
        if(mijson.result == 'Pago realizado correctamente'){
            fetch(queURLV, {
                method: 'PUT',
                body: JSON.stringify(reservado),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`                                   
                         }
        
                })
            .then( resp => resp.json() )
            
            elemento = {
                Primer_Paso: elemento.Primer_Paso,
                InicioTiempo: elemento.InicioTiempo,
                id: 'El vehiculo ' + elemento.id + ' esta reservado',
                pagado: 'Pagado'
            };
            coleccion.save(elemento, (err, elementoGuardado) =>{
                if(err) return next(err);
                
                res.json({
                    result: 'OK',
                    coleccion: 'transacciones',
                    elemento: elementoGuardado
                });
            });
        }
        else{
            fetch(queURLV, {
                method: 'PUT',
                body: JSON.stringify(libre),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`                                   
                         }
        
                })
            .then( resp => resp.json() )
            elemento = {
                Primer_Paso: elemento.Primer_Paso,
                InicioTiempo: elemento.InicioTiempo,
                id: 'El vehiculo ' + elemento.id + ' ha sido liberado',
                pagado: 'saldo insuficiente'
            };
            coleccion.save(elemento, (err, elementoGuardado) =>{
                if(err) return next(err);
                
                res.json({
                    result: 'OK',
                    coleccion: 'transacciones',
                    elemento: elementoGuardado
                });
            });
        }
    } );
});

app.post('/api/transacciones/reservarHotel', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = req.body.id;

    const prerreservado = {estado: 'prerreservado'};
    const reservado = {estado: 'reservado'};
    const libre = {estado: 'libre'};
    const queURLH = `${URL_HOTELES}/${idElemento}`;
    const queURLP = `${URL_PAGOS}`;
    const queToken = req.headers.authorization.split(" ")[1];

    console.log(elemento);
    //Añado el estado prerreservado al hotel dado por el idElemento
    fetch(queURLH, {
        method: 'PUT',
        body: JSON.stringify(prerreservado),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`                                   
        }

    }).then( resp => resp.json() )

    fetch(queURLP)
    .then(resp => resp.json() )
    .then(mijson => 
    {
        if(mijson.result == 'Pago realizado correctamente'){
            fetch(queURLH, {
                method: 'PUT',
                body: JSON.stringify(reservado),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`                                   
                         }
        
                })
            .then( resp => resp.json() )
            
            elemento = {
                Primer_Paso: elemento.Primer_Paso,
                InicioTiempo: elemento.InicioTiempo,
                id: 'El hotel ' + elemento.id + ' esta reservado',
                pagado: 'Pagado'
            };
            coleccion.save(elemento, (err, elementoGuardado) =>{
                if(err) return next(err);
                
                res.json({
                    result: 'OK',
                    coleccion: 'transacciones',
                    elemento: elementoGuardado
                });
            });
        }
        else{
            fetch(queURLH, {
                method: 'PUT',
                body: JSON.stringify(libre),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`                                   
                         }
        
                })
            .then( resp => resp.json() )
            elemento = {
                Primer_Paso: elemento.Primer_Paso,
                InicioTiempo: elemento.InicioTiempo,
                id: 'El hotel ' + elemento.id + ' ha sido liberado',
                pagado: 'saldo insuficiente'
            };
            coleccion.save(elemento, (err, elementoGuardado) =>{
                if(err) return next(err);
                
                res.json({
                    result: 'OK',
                    coleccion: 'transacciones',
                    elemento: elementoGuardado
                });
            });
        }
    } );
});

app.post('/api/transacciones/reservarVuelo', auth, (req, res, next) => {
    let elemento = req.body;
    const idElemento = req.body.id;

    const prerreservado = {estado: 'prerreservado'};
    const reservado = {estado: 'reservado'};
    const libre = {estado: 'libre'};
    const queURLV = `${URL_VUELOS}/${idElemento}`;
    const queURLP = `${URL_PAGOS}`;
    const queToken = req.headers.authorization.split(" ")[1];

    console.log(elemento);
    //Añado el estado prerreservado al vuelo dado por el idElemento
    fetch(queURLV, {
        method: 'PUT',
        body: JSON.stringify(prerreservado),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`                                   
        }

    }).then( resp => resp.json() )

    fetch(queURLP)
    .then(resp => resp.json() )
    .then(mijson => 
    {
        if(mijson.result == 'Pago realizado correctamente'){
            fetch(queURLV, {
                method: 'PUT',
                body: JSON.stringify(reservado),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`                                   
                         }
        
                })
            .then( resp => resp.json() )
            
            elemento = {
                Primer_Paso: elemento.Primer_Paso,
                InicioTiempo: elemento.InicioTiempo,
                id: 'El vuelo ' + elemento.id + ' esta reservado',
                pagado: 'Pagado'
            };
            coleccion.save(elemento, (err, elementoGuardado) =>{
                if(err) return next(err);
                
                res.json({
                    result: 'OK',
                    coleccion: 'transacciones',
                    elemento: elementoGuardado
                });
            });
        }
        else{
            fetch(queURLV, {
                method: 'PUT',
                body: JSON.stringify(libre),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${queToken}`                                   
                         }
        
                })
            .then( resp => resp.json() )
            elemento = {
                Primer_Paso: elemento.Primer_Paso,
                InicioTiempo: elemento.InicioTiempo,
                id: 'El vuelo ' + elemento.id + ' ha sido liberado',
                pagado: 'saldo insuficiente'
            };
            coleccion.save(elemento, (err, elementoGuardado) =>{
                if(err) return next(err);
                
                res.json({
                    result: 'OK',
                    coleccion: 'transacciones',
                    elemento: elementoGuardado
                });
            });
        }
    } );
});

//Devolver un elemento concreto de una tabla concreta
app.get('/api/transacciones/:id', (req, res, next) => {
    const queId = req.params.id

    coleccion.findOne({_id: id(queId)},(err, elemento) => {
        if (err) return next(err);

        console.log(elemento);
        res.json({
            result: 'OK',
            colecciones: 'transacciones',
            elemento: elemento
        });
    });
});

//Añadir elementos a una colección(tabla)
app.post('/api/transacciones', auth, (req, res, next) => {
    const nuevoElemento = req.body;

    coleccion.save(nuevoElemento, (err, elementoGuardado) => {
        if (err) return next(err);

        console.log(elementoGuardado);
        res.status(201).json({
            result: 'OK',
            colección: 'transacciones',
            elemento: elementoGuardado
        });
    });
});

app.put('/api/transacciones/:id', auth, (req, res, next) => {
    const nuevosDatos = req.body;
    const queId = req.params.id;

    coleccion.update(
        {_id: id(queId)}, 
        {$set: nuevosDatos},
        {safe: true, multi: false},
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'transacciones',
                resultado: resultado
            });
        }
    );
});

app.delete('/api/transacciones/:id', auth, (req, res, next) => {
    let queId = req.params.id;

    coleccion.remove(
        {_id: id(queId)}, 
        (err, resultado)  => {
            if (err) return next(err);
            console.log(resultado);
            res.json({
                result: 'OK',
                colección: 'transacciones',
                elemento: queId,
                resultado: resultado
            });
        }
    );
});

https.createServer(OPTIONS_HTTPS, app).listen(port, () => {
    console.log(`WS API REST DE TRANSACCIONES ejecutándose en https://localhost:${port}/api/:colecciones/:id`);
});

