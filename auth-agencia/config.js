module.exports={
    port: process.env.PORT || 3000,
    db: process.env.MONGODB || 'mongodb://localhost:27017/AGENCIA',
    SECRET: 'miclavesecretadetokenamodificar',
    TOKEN_EXP_TIME: 7*24*60
};
